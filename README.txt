# Roles Machine Name
This module basically displays a role machine name at the bottom of each role on the roles page.

## Requirements
- Enabled core module: User.

## Setup
No setup is needed just visit the admin role page and enjoy.

## Installation
Install as usual, see:
  https://www.drupal.org/docs/extending-drupal/installing-modules
  for further information.

## Maintainers
Current maintainers:
- Mohammed Bamlhes (bamlhes) - https://www.drupal.org/u/bamlhes

